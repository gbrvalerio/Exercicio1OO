#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP

#include <iostream>
#include "pessoa.hpp"

using namespace std;

class Professor : public Pessoa {
private:
	string formacao;
	double salario;
	string sala;
public:
	Professor();
	~Professor();
	string getFormacao();
	void setFormacao(string formacao);
	double getSalario();
	void setSalario(double salario);
	string getSala();
	void setSala(string sala);
	void imprimeDados();
};

#endif
