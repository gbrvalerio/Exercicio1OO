#include <iostream>
#include <vector>
#include <cstdlib>
#include "professor.hpp"

#define TOTAL 3

using namespace std;

int main(int argc, char ** argv) {

   /*Pessoa pessoa_1;
   Pessoa pessoa_2("Maria", "555-1111", 21);

   Pessoa * pessoa_3;
   pessoa_3 = new Pessoa();
 
   Pessoa * pessoa_4;
   pessoa_4 = new Pessoa("Marcelo", "666-7777", 25);   
  
   pessoa_1.setNome("Joao");
   pessoa_1.setMatricula("14/0078070");
   pessoa_1.setTelefone("555-5555");
   pessoa_1.setSexo("M");
   pessoa_1.setIdade(20);
   
   pessoa_3->setNome("Pateta");
   pessoa_3->setMatricula("10/12312313");
   pessoa_3->setTelefone("4444-1111");
   pessoa_3->setSexo("M");
   pessoa_3->setIdade(12);
   

   //cout << "Nome: " << pessoa_1.nome; << endl;
   cout << "Nome: " << pessoa_1.getNome() << endl;
   cout << "Matrícula: " << pessoa_1.getMatricula() << endl;
   cout << "Telefone: " << pessoa_1.getTelefone() << endl;
   cout << "Sexo: " << pessoa_1.getSexo() << endl;
   cout << "Idade: " << pessoa_1.getIdade() << endl;

   cout << endl;
   cout << "Nome: " << pessoa_2.getNome() << endl;
   cout << "Telefone: " << pessoa_2.getTelefone() << endl;
   cout << "Idade: " << pessoa_2.getIdade() << endl;
   pessoa_1.imprimeDados();
   pessoa_2.imprimeDados();
   pessoa_3->imprimeDados();
   pessoa_4->imprimeDados();*/

   vector<Professor *> professores = vector<Professor *>();

   for(int i = 0; i < TOTAL; i++){
      Professor *professor = new Professor();

      string temp_str;
      double temp_double;

      cout << "Digite o nome do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setNome(temp_str);
      cout << "Digite a matricula do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setMatricula(temp_str);
      cout << "Digite o sexo do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setSexo(temp_str);
      cout << "Digite a idade do " << i+1 << "o professor: ";
      cin >> temp_double;
      professor->setIdade((int) temp_double);
      cout << "Digite o telefone do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setTelefone(temp_str);
      cout << "Digite a formacao do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setFormacao(temp_str);
      cout << "Digite o salario do " << i+1 << "o professor: ";
      cin >> temp_double;
      professor->setSalario(temp_double);
      cout << "Digite a sala do " << i+1 << "o professor: ";
      cin >> temp_str;
      professor->setSala(temp_str);

      professores.push_back(professor);
      system("clear");
   }

   for(int i = 0; i < TOTAL; i++){
      professores[i]->imprimeDados();
      delete(professores[i]);
   }
 
   return 0;
}
