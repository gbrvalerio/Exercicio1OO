#include "professor.hpp"

using namespace std;

Professor::Professor(){
	setNome("");
	setMatricula("");
	setIdade(0);
	setSexo("");
	setTelefone("");
}

Professor::~Professor(){}

string Professor::getFormacao(){
	return this->formacao;
}

void Professor::setFormacao(string formacao){
	this->formacao = formacao;
}

double Professor::getSalario(){
	return this->salario;
}

void Professor::setSalario(double salario){
	this->salario = salario;
}

string Professor::getSala(){
	return this->sala;
}

void Professor::setSala(string sala){
	this->sala = sala;
}

void Professor::imprimeDados() {
	Pessoa::imprimeDados();
	cout << "Formacao: " << getFormacao() << endl;
   	cout << "Salario: " << getSalario() << endl;
   	cout << "Sala: " << getSala() << endl << endl;
}